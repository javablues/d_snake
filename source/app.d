module app;

void main() {
    import game;
    import game_state;
    import raylib : WindowShouldClose;

    auto snakeGame = Game("Snake game in D programming language");

    while (snakeGame.state != GameState.close) {
        if (snakeGame.state == GameState.run) {
            snakeGame.run();
        }
        if (snakeGame.state == GameState.stop) {
            snakeGame.gameOver();
        }
        if (WindowShouldClose()) {
            snakeGame.state = GameState.close;
        }
    }

}
