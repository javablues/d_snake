module direction;

enum Direction
{
    up,
    down,
    left,
    right
}
