module snake;

struct Snake
{
    import raylib;
    import direction;

    Rectangle[16] snakeBody;
    Colors color;
    Direction snakeDirection = Direction.up;
    int windowSize;
    int blockSize;
    int snakeLength = 5;

    this(int windowSize, int blockSize, Colors color) {
        this.windowSize = windowSize;
        this.blockSize = blockSize;
        this.color = color;
        snakeBody = Rectangle(-blockSize, -blockSize, blockSize, blockSize);
        snakeBody[0] = Rectangle(windowSize / 2, windowSize / 2, blockSize, blockSize);
    }

    void processInput() {
        if (IsKeyPressed(KeyboardKey.KEY_UP))
            snakeDirection = Direction.up;
        else if (IsKeyPressed(KeyboardKey.KEY_DOWN))
            snakeDirection = Direction.down;
        else if (IsKeyPressed(KeyboardKey.KEY_LEFT))
            snakeDirection = Direction.left;
        else if (IsKeyPressed(KeyboardKey.KEY_RIGHT))
            snakeDirection = Direction.right;
    }

    void update() {
        // move tail
        for (int i = snakeLength; i > 0; i--) {
            snakeBody[i].x = snakeBody[i - 1].x;
            snakeBody[i].y = snakeBody[i - 1].y;
        }

        // move snake
        if (snakeDirection == Direction.up)
            snakeBody[0].y -= blockSize;
        else if (snakeDirection == Direction.down)
            snakeBody[0].y += blockSize;
        else if (snakeDirection == Direction.left)
            snakeBody[0].x -= blockSize;
        else if (snakeDirection == Direction.right)
            snakeBody[0].x += blockSize;

        // check if snake is outside of window
        if (snakeBody[0].x < 0)
            snakeBody[0].x = windowSize - blockSize;
        else if (snakeBody[0].x > windowSize)
            snakeBody[0].x = 0;
        else if (snakeBody[0].y < 0)
            snakeBody[0].y = windowSize - blockSize;
        else if (snakeBody[0].y > windowSize)
            snakeBody[0].y = 0;
    }

    void grow() {
        if (snakeLength < snakeBody.length - 1)
            ++snakeLength;
    }

    void render() {
        for (int i = 0; i < snakeLength; i++) {
            DrawRectangleRec(snakeBody[i], color);
        }
    }

}
