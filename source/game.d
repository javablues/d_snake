module game;

struct Game
{
    immutable int windowSize = 640;
    immutable int blockSize = 32;

    import std.string;

    import raylib;
    import grid;
    import fruit;
    import snake;
    import game_state;

    Grid gameGrid;
    Fruit gameFruit;
    Snake gameSnake;
    GameState state = GameState.run;
    double time = 0;
    double faster = 0.10;

    this(string title) {
        InitWindow(windowSize, windowSize, title.toStringz);
        SetTargetFPS(60);
        gameGrid = Grid(windowSize, blockSize, Colors.DARKGRAY);
        gameFruit = Fruit(windowSize, blockSize, Colors.GREEN);
        gameSnake = Snake(windowSize, blockSize, Colors.RED);
    }

    ~this() {
        CloseWindow();
    }

    void run() {
        processInput();
        update();
        render();
    }

    void processInput() {
        gameSnake.processInput();
    }

    void update() {
        time += GetFrameTime();
        if (time > faster) {
            time = 0;
            gameFruit.update();
            gameSnake.update();

            // snake eats itself?
            for (int i = 1; i < gameSnake.snakeLength; i++) {
                if (gameSnake.snakeBody[0].x == gameSnake.snakeBody[i].x
                        && gameSnake.snakeBody[0].y == gameSnake.snakeBody[i].y) {
                    state = GameState.stop;
                }
            }

            // snake eats fruit?
            if (gameSnake.snakeBody[0].x == gameFruit.fruitShape.x
                    && gameSnake.snakeBody[0].y == gameFruit.fruitShape.y) {
                gameFruit.eaten = true;
                gameSnake.grow();
                makeGameRunFaster();
            }
        }
    }

    void makeGameRunFaster() {
        if (faster > 0.025) {
            faster = faster - 0.005;
        }
    }

    void render() {
        BeginDrawing();
        ClearBackground(Colors.BLACK);
        gameGrid.render();
        gameFruit.render();
        gameSnake.render();
        EndDrawing();
    }

    void gameOver() {
        BeginDrawing();
        ClearBackground(Colors.BLACK);
        DrawText("Game Over", 170, 260, 60, Colors.RED);

        import std.format;
        import std.string : toStringz;

        DrawText(format("eaten: %d", gameFruit.quantity).toStringz, 10,
                windowSize - 30, 30, Colors.BLUE);
        EndDrawing();
    }

}
