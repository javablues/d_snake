module grid;

struct Grid
{
    import raylib;

    Colors color;
    int windowSize;
    int blockSize;

    this(int windowSize, int blockSize, Colors color) {
        this.windowSize = windowSize;
        this.blockSize = blockSize;
        this.color = color;
    }

    void render() {
        for (int i = 1; i < windowSize / blockSize; i++) {
            DrawLineEx(Vector2(blockSize * i, 0), Vector2(blockSize * i, windowSize), 1, color);
            DrawLineEx(Vector2(0, blockSize * i), Vector2(windowSize, blockSize * i), 1, color);
        }
    }
}
