module fruit;

struct Fruit
{
    import raylib;

    Rectangle fruitShape;
    Colors color;
    int windowSize;
    int blockSize;
    int quantity;
    bool eaten;

    this(int windowSize, int blockSize, Colors color) {
        this.windowSize = windowSize;
        this.blockSize = blockSize;
        this.color = color;
        this.eaten = true;
        this.quantity = -1;
    }

    void update() {
        if (eaten) {
            generateFruit();
            eaten = false;
            ++quantity;
        }
    }

    void generateFruit() {
        import std.random : uniform;

        int x = uniform(0, windowSize / blockSize) * blockSize;
        int y = uniform(0, windowSize / blockSize) * blockSize;
        fruitShape = Rectangle(x, y, blockSize, blockSize);
    }

    void showEatenQty() {
        import std.format;
        import std.string : toStringz;

        DrawText(format("eaten: %d", quantity).toStringz, 10, windowSize - 30, 30, Colors.BLUE);
    }

    void render() {
        showEatenQty();
        if (!eaten) {
            DrawRectangleRec(fruitShape, color);
        }
    }

}
