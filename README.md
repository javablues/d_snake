# Snake game in D programming language

## How to run it?

### Under Windows

1. Download and install `DMD` from (https://dlang.org/download.html)
2. Clone repo or download zip file (https://github.com/slawtul/d_snake/archive/refs/heads/main.zip)
3. Open `D2 64-bit Command Prompt`
4. Go to `d_snake` folder
5. Run `dub` command

```bash
cd C:\repos\d_snake
dub
```

### Under Linux

Install raylib via cmake (description on wiki)
and do not forget to launch: sudo ldconfig
